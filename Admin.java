/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajante_practice;

/**
 *
 * @author Cesar Galvis
 */
public class Admin {
    int level;

    public void setLevel(int level) {
        this.level = level;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getLevel() {
        return level;
    }

    public double getRating() {
        return rating;
    }
    double rating;
}
