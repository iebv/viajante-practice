package viajante-practice;

import java.util.Date;

/**
 * TODO Put here a description of what this class does.
 *
 * @author Wiston Forero Corba.
 *         Created 13/09/2014.
 */
public class Forum {

	/**
	 * TODO Put here a description of what this constructor does.
	 *
	 */
	public Forum() {
		super();
		// TODO Auto-generated constructor stub.
	}
	/**
	 * TODO Put here a description of what this method does.
	 *
	 * @param args
	 */
	private String name;
    private Date dateCreated;
    private String category;
    
    
	/**
	 * Returns the value of the field called 'name'.
	 * @return Returns the name.
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * Sets the field called 'name' to the given value.
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Returns the value of the field called 'dateCreated'.
	 * @return Returns the dateCreated.
	 */
	public Date getDateCreated() {
		return this.dateCreated;
	}
	/**
	 * Sets the field called 'dateCreated' to the given value.
	 * @param dateCreated The dateCreated to set.
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	/**
	 * Returns the value of the field called 'category'.
	 * @return Returns the category.
	 */
	public String getCategory() {
		return this.category;
	}
	/**
	 * Sets the field called 'category' to the given value.
	 * @param category The category to set.
	 */
	public void setCategory(String category) {
		this.category = category;
	}
    

}