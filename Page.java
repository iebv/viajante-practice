/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viajante_practice;

/**
 *
 * @author Cesar Galvis
 */
public class Page {
    int[] size;

    public void setSize(int[] size) {
        this.size = size;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int[] getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
    String name;
    String color;
}
