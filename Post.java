package viajante_practice;

import java.util.Date;

public class Post {
    
    private String topic;
    private Date dateCreated;
    private Date lastUpdate;
    private boolean isAllowed;

    public Post(String topic, Date dateCreated, Date lastUpdate, boolean isAllowed) {
        this.topic = topic;
        this.dateCreated = dateCreated;
        this.lastUpdate = lastUpdate;
        this.isAllowed = isAllowed;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isIsAllowed() {
        return isAllowed;
    }

    public void setIsAllowed(boolean isAllowed) {
        this.isAllowed = isAllowed;
    }
    
    public void comment(){
        
    }
    
    public void rate(){
        
    }
    
    public void share(){
        
    }
}
